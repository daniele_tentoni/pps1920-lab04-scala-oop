package u04lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u04lab.code.Lists.List._

class ListsTests {

  @Test def testStudentCourses: Unit = {
    val cPPS = Course("PPS","Viroli")
    val cPCD = Course("PCD","Ricci")
    val s1 = Student("mario",2015)
    assertEquals(
      Nil(),
      s1.courses
    )
    s1.enrolling(cPPS)
    s1.enrolling(cPCD)
    assertEquals(
      Cons("PCD", Cons("PPS", Nil())),
      s1.courses
    )
  }

  @Test def testStudentHasTeacher: Unit = {
    val cPPS = Course("PPS","Viroli")
    val cPCD = Course("PCD","Ricci")
    val s1 = Student("mario",2015)
    s1.enrolling(cPPS)
    s1.enrolling(cPCD)
    assertTrue(s1.hasTeacher("Ricci"))
  }

  @Test def testCoursesArray: Unit = {
    val cPPS = Course("PPS","Viroli")
    val cPCD = Course("PCD","Ricci")
    val s1 = Student("mario",2015)
    val courses = Cons(cPPS, Cons(cPCD, Nil()))
    assertEquals(
      Nil(),
      s1.courses
    )
    s1.enrolling(cPPS, cPCD)
    assertEquals(
      Cons("PCD", Cons("PPS", Nil())),
      s1.courses
    )
  }
}
